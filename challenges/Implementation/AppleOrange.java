import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

/**
Hacker Rank problem: https://www.hackerrank.com/challenges/apple-and-orange
**/
public class AppleOrange {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int t = in.nextInt();
        in.nextLine();
        int a = in.nextInt();
        int b = in.nextInt();
        in.nextLine();
        int m = in.nextInt();
        int n = in.nextInt();
        in.nextLine();
        int countA=0,countB = 0;
        int[] apple = new int[m];
        for(int apple_i=0; apple_i < m; apple_i++){
            apple[apple_i] = in.nextInt();
            if(a+apple[apple_i]>0){
                if(a+apple[apple_i]>=s && a+apple[apple_i]<=t){
                    countA++;
                }
            }
            
        }
        int[] orange = new int[n];
        for(int orange_i=0; orange_i < n; orange_i++){
            orange[orange_i] = in.nextInt();
            if(b+orange[orange_i]>0){
                if(b+orange[orange_i]>=s && b+orange[orange_i]<=t){
                    countB++;
                }
            }
        }
        System.out.println(countA);
        System.out.println(countB);
    }
}