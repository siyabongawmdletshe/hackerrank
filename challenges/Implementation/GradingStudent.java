import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

/**
  Hacker Rank problem : https://www.hackerrank.com/challenges/grading
**/

public class GradingStudent {

    public static int getGrade(int grade){
        boolean found = false;
        int d = 0;
        int multiple = grade;
         if(grade<38){
         return grade;
         }
        else{
           while(!found){
            multiple++;
           if(multiple%5==0){
               found = true;
               d = multiple - grade;
               if(d<3){
                  grade = multiple;
                }
           }
          }
         return grade;
        }
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int a0 = 0; a0 < n; a0++){
            int grade = in.nextInt();
            System.out.println(getGrade(grade));
        }
    }
}