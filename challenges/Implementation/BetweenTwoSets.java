import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

/**
The hacker rank problem is here https://www.hackerrank.com/challenges/between-two-sets?h_r=next-challenge&h_v=zen 
**/

public class BetweenTwoSets {

    //Check if all elements in A are factors of x.
    public static boolean areAllxFactors(int x,int [] a){
        boolean allfactorOfX = true;
        int size = a.length;
            for(int j=0; j<size; j++){
                if(x%a[j]!=0){
                     allfactorOfX = false;
                     break;
                }
            }
        return allfactorOfX;
    }
    //Check if x is a factor of all elements in B.
    public static boolean isXaFactorOfAll(int x,int [] b){
        int size = b.length;
        boolean factorForAll= true;
        for(int k=0; k < size; k++){
               if(b[k]%x!=0){
                   factorForAll=false;
                   break;
               }         
        }
        return factorForAll;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] a = new int[n];
        int count= 0;
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int[] b = new int[m];
        for(int b_i=0; b_i < m; b_i++){
            b[b_i] = in.nextInt();
        }
        
        Arrays.sort(a);
        Arrays.sort(b);
        int bsize=b.length-1;
        
        //Print the number of integers that are considered to be between A and B.
        for(int i=1; i <= b[bsize]; i++){
           int x = i;
            if(areAllxFactors(x,a)){
                if(isXaFactorOfAll(x,b)){
                    count++;
                }
            }
        }
        System.out.println(count);
       
    }
}
