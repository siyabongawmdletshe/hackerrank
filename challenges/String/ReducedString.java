import java.util.*;
import java.lang.*;
import java.io.*;

/*The hack rank problem: https://www.hackerrank.com/challenges/reduced-string*/

public class ReducedString
{
  
  public static void main(String[] args)
  {
    
     Scanner kb = new Scanner(System.in);
       String s = kb.next(); 
       StringBuilder myName = new StringBuilder(s);
       int minIndex = 0;
       int maxIndex = minIndex+1;
       while(myName.length() >1 && maxIndex<myName.length()){
           
           if(myName.charAt(minIndex)==myName.charAt(maxIndex)){
               myName.deleteCharAt(minIndex);
               myName.deleteCharAt(--maxIndex);
               minIndex = 0;
               maxIndex = minIndex+1;
           }
          else{minIndex++;maxIndex=minIndex+1;}
       }
      System.out.println(myName.length()!=0?myName:"Empty String");
  }
}